import React, {Component} from 'react';

class PhoneForm extends Component {
    state = {
        name : '',
        phone: ''
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    handleSubmit = (e) => {

        // 원래는 form 에서 submit 이 발생하면 
        // 페이지를 다시 불러오게 되는데요, 
        // 그렇게 되면 우리가 지니고있는 상태를 
        // 다 잃어버리게 되니까 이를 통해서 방지해주었습니다.
        e.preventDefault();

        // -------------------------
        // 부모의 method를 호출하여 
        // 자식의 state값을 전달함.
        // -------------------------
        this.props.onCreate(this.state);
        this.setState({ name : '', phone : '' })
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <input 
                    placeholder="이름"
                    onChange={this.handleChange}
                    value={this.state.name}
                    name="name"
                />
                <input 
                    placeholder="전번"
                    onChange={this.handleChange}
                    value={this.state.phone}
                    name="phone"
                />
                <div>Name : {this.state.name}</div>
                <div>Phone : {this.state.phone}</div>
                <button type="submit">등록</button>
            </form>
        )
    }
}

export default PhoneForm;